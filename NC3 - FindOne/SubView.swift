//
//  SubView.swift
//  NC3 - FindOne
//
//  Created by Maurizio Minieri on 03/03/2020.
//  Copyright © 2020 Maurizio Minieri. All rights reserved.
//

import SwiftUI

struct Subview: View {
	
	var imageString: String
	
	var body: some View {
		
		HStack(alignment: .center){
			
			Image(imageString)
				.resizable()
				.aspectRatio(contentMode: .fill)
				.clipped()
			
		}
		
	}
}


struct Subview_Previews: PreviewProvider {
	static var previews: some View {
		ForEach(["iPhone SE", "iPhone Xs Max", "iPad Pro (11-inch)"], id: \.self) { deviceName in
			
			Group{
			Subview(imageString: "event")
					.previewDevice(PreviewDevice(rawValue: deviceName))
					.environment(\.colorScheme, .light)
					.previewDisplayName(deviceName)
				
				Subview(imageString: "event")
					.previewDevice(PreviewDevice(rawValue: deviceName))
					.environment(\.colorScheme, .dark)
					.previewDisplayName(deviceName)
			}
		}
	}
}

