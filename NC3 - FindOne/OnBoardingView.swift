//
//  OnBoardingView.swift
//  NC3 - FindOne
//
//  Created by Maurizio Minieri on 03/03/2020.
//  Copyright © 2020 Maurizio Minieri. All rights reserved.
//

import SwiftUI

struct OnboardingView: View {
	
	@ObservedObject var viewRouter: ViewRouter
	
	var subviews = [
		UIHostingController(rootView: Subview(imageString: "event")),
		UIHostingController(rootView: Subview(imageString: "player")),
		UIHostingController(rootView: Subview(imageString: "near"))
	]
	
	var titles = ["Create your event","Discover all the events that surround you","Join the nearest game"]
	
	var captions =  ["Take your time out and bring awareness into your everyday life", "Meditating helps you dealing with anxiety and other psychic problems", "Regular medidation sessions creates a peaceful inner mind"]
	
	@State var currentPageIndex = 0
	@State private var isActive: Bool = false
	@State private var selection: Int? = nil
	
	
	var body: some View {
		
		//NavigationView{
			
			VStack {
				
				PageViewController(currentPageIndex: $currentPageIndex, viewControllers: subviews)
					.frame(height: 500)
				Group {
					Text(titles[currentPageIndex])
						.padding(.top, -50)
					// .font(.title)
					Text(captions[currentPageIndex])
						.font(.subheadline)
						.foregroundColor(.gray)
						.frame(width: 300, height: 50, alignment: .leading)
						.lineLimit(nil)
				}
				.padding(.top, -55)
				
				HStack {
					
					PageControl(numberOfPages: subviews.count, currentPageIndex: $currentPageIndex)
					
					Spacer()
					
					
					
					Button(action: {
						if self.currentPageIndex+1 == self.subviews.count {
						
							self.viewRouter.currentPage = "page2"
							
						}
						else {
							
							self.currentPageIndex += 1
						}
						
					}) {
						
						ButtonContent()
						
					}
				}
				.padding()
			}
	}
}

struct ButtonContent: View {
	var body: some View {
		Image(systemName: "arrow.right")
			.resizable()
			.foregroundColor(.white)
			.frame(width: 30, height: 30)
			.padding()
			.background(Color(red: 10 / 255, green: 157 / 255, blue: 124 / 255))
			//.background(Color.orange)
			.cornerRadius(30)
	}
}


struct OnboardingView_Previews: PreviewProvider {
	static var previews: some View {
		ForEach(["iPhone SE", "iPhone Xs Max", "iPad Pro (11-inch)"], id: \.self) { deviceName in
			
			Group{
				OnboardingView(viewRouter: ViewRouter())
					.previewDevice(PreviewDevice(rawValue: deviceName))
					.environment(\.colorScheme, .light)
					.previewDisplayName(deviceName)
		
				OnboardingView(viewRouter: ViewRouter())
					.previewDevice(PreviewDevice(rawValue: deviceName))
					.environment(\.colorScheme, .dark)
					.previewDisplayName(deviceName)
			}
		}
	}
}
