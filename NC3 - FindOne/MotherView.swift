//
//  MotherView.swift
//  NC3 - FindOne
//
//  Created by Maurizio Minieri on 10/03/2020.
//  Copyright © 2020 Maurizio Minieri. All rights reserved.
//

import Foundation
import SwiftUI
import Combine


class ViewRouter: ObservableObject {
	
	let objectWillChange = PassthroughSubject<ViewRouter,Never>()
	
	var currentPage: String = "page1" {
		didSet {
			objectWillChange.send(self)
		}
	}
}


//Questa contiene l'onboarding e la view che contiene la tabview
struct MotherView: View {
	
	@ObservedObject var viewRouter: ViewRouter
	@ObservedObject var viewRouter2: ViewRouter2
	@State var checkpoints: [LandmarkAnnotation] = [
		LandmarkAnnotation(title: "Napoli2", subtitle: "Bello2", coordinate: .init(latitude: 40.8765005, longitude: 14.1303887)),
		LandmarkAnnotation(title: "Stadio", subtitle: "Che figooooo, che campo wauuu", coordinate: .init(latitude: 40.828845, longitude: 15.194173))
	]
	
	//fields in cui creare un evento, (visibili col tasto +)
	@State var checkpointsPlus: [LandmarkAnnotation] = [
		LandmarkAnnotation(title: "Napoli", subtitle: "Bello", coordinate: .init(latitude: 40.8702996, longitude: 15.1679429)),
		LandmarkAnnotation(title: "Mergellina", subtitle: "Bello wau che bello sto campo, waaa", coordinate: .init(latitude: 40.834506, longitude: 15.237417))
	]
	
	@State var showingProfile = false
	
	var profileButton: some View {
		Button(action: { self.showingProfile.toggle() }) {
			Image(systemName: "person.crop.circle")
				.imageScale(.large)
				.accessibility(label: Text("User Profile"))
				.padding()
		}
	}
	
	var body: some View {
		
		VStack {
			
			if viewRouter.currentPage == "page1" {
				//MapView(viewRouter: viewRouter, checkpoints: $checkpoints)
				OnboardingView(viewRouter: viewRouter)
			}
			else if viewRouter.currentPage == "page2" {
			
				ContentView(viewRouter: viewRouter,viewRouter2: viewRouter2)
			
			}
			else{
				FieldDetail(field:fieldData[0],event:fieldData[0].events[0],viewRouter2: viewRouter2)
			}
			
			
		}
		
	}
	
}


struct MotherView_Previews : PreviewProvider {
	static var previews: some View {
		ForEach(["iPhone SE", "iPhone Xs Max", "iPad Pro (11-inch)"], id: \.self) { deviceName in
			
			Group{
				MotherView(viewRouter: ViewRouter(), viewRouter2: ViewRouter2())
					.previewDevice(PreviewDevice(rawValue: deviceName))
					.environment(\.colorScheme, .light)
					.previewDisplayName(deviceName)
				
				MotherView(viewRouter: ViewRouter(), viewRouter2: ViewRouter2())
					.previewDevice(PreviewDevice(rawValue: deviceName))
					.environment(\.colorScheme, .dark)
					.previewDisplayName(deviceName)
			}
		}
	}
}
