//
//  MapViewOfficial.swift
//  NC3 - FindOne
//
//  Created by Maurizio Minieri on 02/03/2020.
//  Copyright © 2020 Maurizio Minieri. All rights reserved.
//

import SwiftUI
import MapKit

struct MapViewOfficial: UIViewRepresentable {
	var coordinate: CLLocationCoordinate2D
	
	func makeUIView(context: Context) -> MKMapView {
		MKMapView(frame: .zero)
	}
	
	func updateUIView(_ view: MKMapView, context: Context) {
		
		let span = MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2)
		let region = MKCoordinateRegion(center: coordinate, span: span)
		print("REGION: \(region)")
		view.setRegion(region, animated: true)
	}
	
}

struct MapView_Previews: PreviewProvider {
	static var previews: some View {
		ForEach(["iPhone SE", "iPhone Xs Max", "iPad Pro (11-inch)"], id: \.self) { deviceName in
			
			Group{
					MapViewOfficial(coordinate: fieldData[0].locationCoordinate)
					.previewDevice(PreviewDevice(rawValue: deviceName))
					.environment(\.colorScheme, .light)
					.previewDisplayName(deviceName)
				
					MapViewOfficial(coordinate: fieldData[0].locationCoordinate)
					.previewDevice(PreviewDevice(rawValue: deviceName))
					.environment(\.colorScheme, .dark)
					.previewDisplayName(deviceName)
			}
		}
	}
}

