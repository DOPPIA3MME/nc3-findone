//
//  CreateGame.swift
//  NC3 - FindOne
//
//  Created by Maurizio Minieri on 03/03/2020.
//  Copyright © 2020 Maurizio Minieri. All rights reserved.
//

import Foundation
import SwiftUI

struct CreateGame: View {
	@State var matchName: String = ""
	@State var dt: String = ""
	@State var email: String = ""
	
	
	
	//Binding: Questo valore sarà fornito da qualche parte, e sarà condiviso tra noi e quel posto.
	@Binding var profile: Profile
	@State private var showingAlert = false
	
	
	var body: some View {
		//Text("Some Content View").navigationBarTitle("Matches")
		
		VStack{
			
			VStack {
				
				
				HStack{
					Image("ball1")
					
					
					TextField("Event Name", text: $matchName)
				.textFieldStyle(RoundedBorderTextFieldStyle())
				}.padding()
				
				HStack{
					Image("calendar1")
						DatePicker(selection: /*@START_MENU_TOKEN@*/.constant(Date())/*@END_MENU_TOKEN@*/, label: { Text(".") })
				}.padding()
				
				
				
				HStack{
					Image("levels")
					Picker("Difficulty", selection: $profile.difficulty) {
						ForEach(Profile.Difficulty.allCases, id: \.self) { season in
							Text(season.rawValue).tag(season)
						}
					}
					.pickerStyle(SegmentedPickerStyle())
				}.padding()
				
				
			}
			.padding(.bottom, 50.0)
			
			
			
			
			Button(action: {self.showingAlert = true}) {
				
				Text("Done")
				
			}
			.alert(isPresented: $showingAlert) {
				Alert(title: Text("Important message"), message: Text("You have created event in this field"), dismissButton: .default(Text("OK!")))}
			
			
			
		}
	}
}


struct CreateGame_Previews: PreviewProvider {
	static var previews: some View {
		ForEach(["iPhone SE", "iPhone Xs Max", "iPad Pro (11-inch)"], id: \.self) { deviceName in
			
			Group{
				CreateGame(profile: .constant(.default))
					.previewDevice(PreviewDevice(rawValue: deviceName))
					.environment(\.colorScheme, .light)
					.previewDisplayName(deviceName)
				
				CreateGame(profile: .constant(.default))
					.previewDevice(PreviewDevice(rawValue: deviceName))
					.environment(\.colorScheme, .dark)
					.previewDisplayName(deviceName)
			}
		}
	}
}
