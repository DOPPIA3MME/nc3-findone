//
//  Profile.swift
//  NC3 - FindOne
//
//  Created by Maurizio Minieri on 24/02/2020.
//  Copyright © 2020 Maurizio Minieri. All rights reserved.
//

import Foundation

struct Profile {
	var username: String
	var prefersNotifications: Bool
	var difficulty: Difficulty
	var goalDate: Date
	
	static let `default` = Self(username: "g_kumar", prefersNotifications: true, difficulty: .hard)
	
	init(username: String, prefersNotifications: Bool = true, difficulty: Difficulty = .hard) {
		self.username = username
		self.prefersNotifications = prefersNotifications
		self.difficulty = difficulty
		self.goalDate = Date()
	}
	
	enum Difficulty: String, CaseIterable {
		case easy = "Easy"
		case medium = "Medium"
		case hard = "Hard"
		//case winter = "☃️"
	}
}
