//
//  Event.swift
//  NC3 - FindOne
//
//  Created by Maurizio Minieri on 03/03/2020.
//  Copyright © 2020 Maurizio Minieri. All rights reserved.
//

import SwiftUI
import CoreLocation


struct Event: Hashable, Codable, Identifiable {
	
	var id: Int
	var name: String
	var date: String
	var time: String
	
}
