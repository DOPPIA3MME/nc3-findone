//
//  CircleImage.swift
//  NC3 - FindOne
//
//  Created by Maurizio Minieri on 02/03/2020.
//  Copyright © 2020 Maurizio Minieri. All rights reserved.
//

import SwiftUI

struct CircleImage: View {
	var image: Image
	
	var body: some View {
		image
			.clipShape(Circle())
			.overlay(Circle().stroke(Color.white, lineWidth: 4))
			.shadow(radius: 10)
	}
}

struct CircleImage_Previews: PreviewProvider {
	static var previews: some View {
		ForEach(["iPhone SE", "iPhone Xs Max", "iPad Pro (11-inch)"], id: \.self) { deviceName in
			
			Group{
			CircleImage(image: Image("icybay"))
					.previewDevice(PreviewDevice(rawValue: deviceName))
					.environment(\.colorScheme, .light)
					.previewDisplayName(deviceName)
				
				CircleImage(image: Image("turtlerock"))
					.previewDevice(PreviewDevice(rawValue: deviceName))
					.environment(\.colorScheme, .dark)
					.previewDisplayName(deviceName)
			}
		}
	}
}
