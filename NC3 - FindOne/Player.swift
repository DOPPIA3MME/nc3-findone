//
//  Player.swift
//  NC3 - FindOne
//
//  Created by Maurizio Minieri on 03/03/2020.
//  Copyright © 2020 Maurizio Minieri. All rights reserved.
//

import SwiftUI
import CoreLocation


struct Player: Hashable, Codable {
	
	var name: String
	var surname: String
	var email: String
	var number: String
	
}
