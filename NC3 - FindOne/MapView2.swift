//
//  MapView2.swift
//  NC3 - FindOne
//
//  Created by Maurizio Minieri on 11/03/2020.
//  Copyright © 2020 Maurizio Minieri. All rights reserved.
//

import SwiftUI
import MapKit
import UIKit



struct MapView2: View {
	@Binding var checkpoints: [LandmarkAnnotation]
	
	var body: some View {
		
		NavigationView{
			MapView(viewRouter: ViewRouter(), checkpoints: $checkpoints)
		}
	}
	
	
	
	
}

