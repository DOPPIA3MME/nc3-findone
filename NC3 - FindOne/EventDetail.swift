//
//  EventDetail.swift
//  NC3 - FindOne
//
//  Created by Maurizio Minieri on 03/03/2020.
//  Copyright © 2020 Maurizio Minieri. All rights reserved.
//

import SwiftUI

struct EventDetail: View {
	var field: Field
	var event: Event
	@State private var showingAlert = false
	
	var body: some View {
		
		VStack{
			
			MapViewOfficial(coordinate: field.locationCoordinate)
				.edgesIgnoringSafeArea(.top)
				.frame(height: 200)
			
			CircleImage(image: field.image)
				.offset(x: 0, y: -130)
				.padding(.bottom, -100)
			
			List{
				Text(event.name).font(.title).multilineTextAlignment(.leading)
				
				VStack(alignment: .center) {
					
					HStack{
						VStack(alignment: .leading){
							HStack{
								Image("calendar")
								Text(event.date)//.font(.subheadline)
							}
							
							HStack{
								Image("clock")
								Text(event.time)//.font(.subheadline)
							}
							
							HStack{
								Image("euro")
								Text(field.price)//.font(.subheadline)
							}
							
						}
						
						Spacer()
						
						VStack(alignment: .leading){
							HStack{
								Image("address")
								Text(field.address)
							}
							
							HStack{
								Image("caution")
								Text(field.capacity)
							}
							
							HStack{
								Image("levels")
								Text("easy")//.font(.subheadline)
							}
							
						}
					}.padding(.horizontal, 40.0)
						.padding(.top,15.0)
					
					
					
					VStack(alignment: .center){
							
						HStack{
							Image("soccer")
							Text("5 vs 5")
							
						}
						Text("Gino Iodice, Maurizio Minieri, Bigb Maione, Stefano Marano, Rino Gattuso are joined!")
							.multilineTextAlignment(.center)
							.padding([.leading, .trailing])
							//.font(.caption)
					}
					.padding(.top, 15.0)
					.padding(.bottom, 40.0)
					
					
					
					
					
				
				}
				
				
				
					
				
			
			}.navigationBarTitle(Text(field.name), displayMode: .inline)
			
			Button(action:{ self.showingAlert = true }) {
				//HStack {
				//Image(systemName: "1.circle")
				//.font(.title)
				Text("I'll play")
					//.fontWeight(.semibold)
					.fontWeight(.semibold)
					.padding()
					.foregroundColor(.white)
					.background(LinearGradient(gradient: Gradient(colors: [Color("DarkGreen"), Color("LightGreen")]), startPoint: .leading, endPoint: .trailing))
					.cornerRadius(40)
			}
			.padding(.bottom, 10.0)
			.alert(isPresented: $showingAlert) {
				Alert(title: Text("Important message"), message: Text("you have been prenoted in this event"), dismissButton: .default(Text("OK!")))}
		}
	}
}


struct EventDetail_Previews: PreviewProvider {
	static var previews: some View {
		ForEach(["iPhone SE", "iPhone Xs Max", "iPad Pro (11-inch)"], id: \.self) { deviceName in
			
			Group{
					EventDetail(field: fieldData[0], event: fieldData[0].events[0])
					.previewDevice(PreviewDevice(rawValue: deviceName))
					.environment(\.colorScheme, .light)
					.previewDisplayName(deviceName)
				
					EventDetail(field: fieldData[0], event: fieldData[0].events[0])
					.previewDevice(PreviewDevice(rawValue: deviceName))
					.environment(\.colorScheme, .dark)
					.previewDisplayName(deviceName)
			}
		}
	}
}

