//
//  FieldRow.swift
//  NC3 - FindOne
//
//  Created by Maurizio Minieri on 03/03/2020.
//  Copyright © 2020 Maurizio Minieri. All rights reserved.
//

import SwiftUI

struct EventRow: View {
	var field: Field
	var event: Event
	
	
	var body: some View {
		HStack {
			field.image
				.resizable()
				.frame(width: 50, height: 50)
				.clipShape(Circle())
				.overlay(Circle().stroke(Color.white, lineWidth: 1))
				.shadow(radius: 5)
			
			VStack(alignment: .leading, spacing: 0){
				Text(event.name).bold()
				HStack{
					Text(event.date).font(.subheadline)
					Text(event.time).font(.subheadline)
					
				}
				Text(field.address)
			}
			
			Spacer()
		}
	}
}


struct EventRow_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			EventRow(field: fieldData[0],event:  fieldData[0].events[0])
			EventRow(field: fieldData[1],event:  fieldData[0].events[0])
		}
		.previewLayout(.fixed(width: 300, height: 70))
	}
}

