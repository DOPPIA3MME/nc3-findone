//
//  File.swift
//  NC3 - FindOne
//
//  Created by Maurizio Minieri on 02/03/2020.
//  Copyright © 2020 Maurizio Minieri. All rights reserved.
//

import SwiftUI
import UIKit
import MapKit
import CoreLocation

struct Field: Hashable, Codable, Identifiable {
	
	var id: Int
	var name: String
	var price: String
	var events: [Event]
	fileprivate var imageName: String
	fileprivate var coordinates: Coordinates
	var state: String
	var address: String
	var capacity: String
	//var isFeatured: Bool
	
	var locationCoordinate: CLLocationCoordinate2D {
		CLLocationCoordinate2D(
			latitude: coordinates.latitude,
			longitude: coordinates.longitude)
	}
}

extension Field {
	var image: Image {
		ImageStore.shared.image(name: imageName)
	}
}

struct Coordinates: Hashable, Codable {
	var latitude: Double
	var longitude: Double
}



