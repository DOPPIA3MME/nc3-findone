//
//  PageControl.swift
//  NC3 - FindOne
//
//  Created by Maurizio Minieri on 03/03/2020.
//  Copyright © 2020 Maurizio Minieri. All rights reserved.
//

import Foundation
import SwiftUI
import UIKit

struct PageControl: UIViewRepresentable {
	
	var numberOfPages: Int
	
	@Binding var currentPageIndex: Int
	
	func makeUIView(context: Context) -> UIPageControl {
		let control = UIPageControl()
		control.numberOfPages = numberOfPages
		control.currentPageIndicatorTintColor =
			
			UIColor.init(red: 0.04, green: 0.62, blue: 0.49, alpha: 1.0)
		control.pageIndicatorTintColor = UIColor.gray
		
		
		
		
		return control
	}
	
	func updateUIView(_ uiView: UIPageControl, context: Context) {
		uiView.currentPage = currentPageIndex
	}
	
}
