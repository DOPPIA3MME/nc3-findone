//
//  LoginView.swift
//  NC3 - FindOne
//
//  Created by Maurizio Minieri on 03/03/2020.
//  Copyright © 2020 Maurizio Minieri. All rights reserved.
//

import Foundation
import SwiftUI

struct LoginView: View {
	
	@State var name: String = ""
	@State var surname: String = ""
	@State var email: String = ""
	@State var number: String = ""
	
	var body: some View {
		VStack{
			
			Text("LOGIN").font(.largeTitle).bold().foregroundColor(Color("DarkGreen"))
			
			Spacer()
			
			TextField("Name", text: $name)
				.textFieldStyle(RoundedBorderTextFieldStyle())
			TextField("Surname", text: $surname)
				.textFieldStyle(RoundedBorderTextFieldStyle())
			TextField("Email", text: $email)
				.textFieldStyle(RoundedBorderTextFieldStyle())
			TextField("Number", text: $number)
				.textFieldStyle(RoundedBorderTextFieldStyle())
			Spacer()
			Button(action: {
				print("Cliccato")
			}) {
				//HStack {
				//Image(systemName: "1.circle")
				//.font(.title)
				Text("Login")
				//.fontWeight(.semibold)
				
				
				//}
				
				
			}	//.padding()
			//.foregroundColor(.black).border(Color.blue, width: 1)
			
			Spacer()
			
		}.padding()
		
	}
	
}


struct LoginView_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			LoginView()
				.environment(\.colorScheme, .light)
			
			LoginView()
				.environment(\.colorScheme, .dark)
		}
	}
}
