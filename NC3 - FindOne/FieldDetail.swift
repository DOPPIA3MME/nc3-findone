//
//  FieldDetail.swift
//  NC3 - FindOne
//
//  Created by Maurizio Minieri on 04/03/2020.
//  Copyright © 2020 Maurizio Minieri. All rights reserved.
//

import Foundation
import SwiftUI

struct FieldDetail: View {
	
	var field: Field
	var event: Event
	@ObservedObject var viewRouter2: ViewRouter2
	
	var body: some View {
		NavigationView{
			VStack{
				
				/*MapViewOfficial(coordinate: field.locationCoordinate)
				.edgesIgnoringSafeArea(.top)
				.frame(height: 250)*/
				
				/*Text(field.name)
				.font(.largeTitle)*/
				field.image
					.resizable()
					//.scaledToFit()
					.frame(height: 300)
				//Spacer()
				
				
				HStack{
					
					VStack{
						Image("ball1")
						NavigationLink(destination: CreateGame(profile: .constant(.default))) {
							Text("Create game")
							
						}
						
					}
					Spacer()
					
					
					
					VStack{
						Image("mapsapple")
						
						Text("Get direction")
							.foregroundColor(.black)
						
						
					}
					Spacer()
					
					
					VStack{
						Image("camera")
						
						Text("Add photo")
							.foregroundColor(.black)
						
						
					}
				}.padding(.horizontal, 40.0)
					.padding(.vertical, 30.0)
				
				
				//List{
				
				
				VStack(alignment: .center) {
					HStack{
						Image("star")
						Text("rating")
						Spacer()
						Text("5/5")
					}.padding(.horizontal, 60.0)
					HStack{
						Image("field")
						Text("field conditions")
						Spacer()
						Text("Excellent")
					}.padding(.horizontal, 60.0)
					HStack{
						Image("metro")
						Text("regular field")
						Spacer()
						Text("No")
					}.padding(.horizontal, 60.0)
					HStack{
						Image("accessibility")
						Text("Disability access")
						Spacer()
						Text("Yes")
					}.padding(.horizontal, 60.0)
					HStack{
						Image("light")
						Text("Night lightning")
						Spacer()
						Text("Yes")
					}.padding(.horizontal, 60.0)
				}.navigationBarTitle(Text(field.name), displayMode: .inline)
				
				
				}
		}
	}
	
}



struct FieldDetail_Previews: PreviewProvider {
	static var previews: some View {
		ForEach(["iPhone SE", "iPhone Xs Max", "iPad Pro (11-inch)"], id: \.self) { deviceName in
			
			Group{
				FieldDetail(field: fieldData[0], event: fieldData[0].events[0], viewRouter2: ViewRouter2())
					.previewDevice(PreviewDevice(rawValue: deviceName))
					.environment(\.colorScheme, .light)
					.previewDisplayName(deviceName)
				
				FieldDetail(field: fieldData[0], event: fieldData[0].events[0], viewRouter2: ViewRouter2())
					.previewDevice(PreviewDevice(rawValue: deviceName))
					.environment(\.colorScheme, .dark)
					.previewDisplayName(deviceName)
			}
		}
	}
}



