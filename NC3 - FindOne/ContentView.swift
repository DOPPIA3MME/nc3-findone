//
//  ContentView.swift
//  FindOne
//
//  Created by Maurizio Minieri on 23/02/2020.
//  Copyright © 2020 Maurizio Minieri. All rights reserved.
//

import SwiftUI
import MapKit
import UIKit
import Combine

class ViewRouter2: ObservableObject {
	
	let objectWillChange = PassthroughSubject<ViewRouter2,Never>()
	
	var currentPage: String = "page1" {
		didSet {
			objectWillChange.send(self)
		}
	}
}


struct ContentView: View {
	
	@ObservedObject var viewRouter: ViewRouter
	@ObservedObject var viewRouter2: ViewRouter2
	
	@State var checkpoints: [LandmarkAnnotation] = [
		LandmarkAnnotation(title: "Napoli2", subtitle: "Bello2", coordinate: .init(latitude: 40.8765005, longitude: 14.1303887)),
		LandmarkAnnotation(title: "Stadio", subtitle: "Che figooooo, che campo wauuu", coordinate: .init(latitude: 40.828845, longitude: 14.194173))
	]
	
	//fields in cui creare un evento, (visibili col tasto +)
	@State var checkpointsPlus: [LandmarkAnnotation] = [
		LandmarkAnnotation(title: "Napoli", subtitle: "Bello", coordinate: .init(latitude: 40.8702996, longitude: 14.1679429)),
		LandmarkAnnotation(title: "Mergellina", subtitle: "Bello wau che bello sto campo, waaa", coordinate: .init(latitude: 40.834506, longitude: 14.237417))
	]
	
	
	
	
	var body: some View {
		//Fornisce anche una navigation view
	
			
			TabView {
				MyEventList(viewRouter: viewRouter, viewRouter2: viewRouter2)
					.tabItem {
						Image(systemName: "list.dash")
						Text("Events")
				}
				
				
				
				
				if(viewRouter2.currentPage == "page1"){
				   MapViewContainer(viewRouter2: viewRouter2)
						.tabItem {
							Image(systemName: "map")
							Text("Fields")}
				}
				else{
					FieldDetail(field:fieldData[0],event:fieldData[0].events[0],viewRouter2: viewRouter2)
						.tabItem {
							Image(systemName: "map")
							Text("Fields")}
				}
				
				
					
					
				
			}
		
			
		
	}
	
	
	
	
}

struct ContentView_Previews: PreviewProvider {
	static var previews: some View {
		ForEach(["iPhone SE", "iPhone Xs Max", "iPad Pro (11-inch)"], id: \.self) { deviceName in
			
			Group{
				ContentView(viewRouter: ViewRouter(), viewRouter2: ViewRouter2())
					.previewDevice(PreviewDevice(rawValue: deviceName))
					.environment(\.colorScheme, .light)
					.previewDisplayName(deviceName)
				
				ContentView(viewRouter: ViewRouter(), viewRouter2: ViewRouter2())
					.previewDevice(PreviewDevice(rawValue: deviceName))
					.environment(\.colorScheme, .dark)
					.previewDisplayName(deviceName)
			}
		}
	}
}

