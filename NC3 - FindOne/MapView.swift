//
//  MapView.swift
//  NC3 - FindOne
//
//  Created by Maurizio Minieri on 26/02/2020.
//  Copyright © 2020 Maurizio Minieri. All rights reserved.
//

import SwiftUI
import MapKit
import UIKit



struct MapViewContainer: View {
	
	@ObservedObject var viewRouter2: ViewRouter2
	
	//fields in cui creare un evento, (visibili col tasto +)
	@State var checkpointsPlus: [LandmarkAnnotation] = [
		LandmarkAnnotation(title: "Napoli", subtitle: "Bello", coordinate: .init(latitude: 40.8702996, longitude: 14.1679429)),
		LandmarkAnnotation(title: "Mergellina", subtitle: "Bello wau che bello sto campo, waaa", coordinate: .init(latitude: 40.834506, longitude: 14.237417))
	]
	
	
	
	@State var showingProfile = false
	
	var profileButton: some View {
		Button(action: { self.showingProfile.toggle() }) {
			Image(systemName: "person.crop.circle")
				.imageScale(.large)
				.accessibility(label: Text("User Profile"))
				.padding()
		}
	}
	
	var plusButton: some View {
		NavigationLink(destination: MapView(viewRouter2:viewRouter2, checkpoints: $checkpointsPlus)) {
			Image(systemName: "plus.circle.fill")
				.font(.largeTitle)
		}.foregroundColor(Color("DarkGreen"))//.buttonStyle(PlainButtonStyle())
	}
	
	var body: some View {
		
		NavigationView{
			VStack{
			MapView(viewRouter2: viewRouter2, checkpoints: $checkpointsPlus)
				
		}.navigationBarTitle(Text("Fields"))
		}
	}

	
}

struct MapView: UIViewRepresentable {
	
	//Con passaggio di array, in modo tale che puoi richiamare la struct MapView con un array come parametro
	
	@ObservedObject var viewRouter2: ViewRouter2
	@State var isActive: Bool = false
	@State var selectedAnnotation: MKAnnotation?
	
	@Binding var checkpoints: [LandmarkAnnotation]
	//let pizzaAnnotations = PizzaAnnotations()
	
	var locationManager = CLLocationManager()
	func setupManager() {
		locationManager.desiredAccuracy = kCLLocationAccuracyBest
		locationManager.requestWhenInUseAuthorization()
		locationManager.requestAlwaysAuthorization()
	}
	
	func makeUIView(context: Context) -> MKMapView {
		
		setupManager()
		let mapView = MKMapView(frame: UIScreen.main.bounds)
		mapView.showsUserLocation = true
		mapView.userTrackingMode = .follow
		return mapView
	}
	
	func makeCoordinator() -> MapViewCoordinator{
		MapViewCoordinator(self, viewRouter2: viewRouter2)
	}
	
	func updateUIView(_ uiView: MKMapView, context: Context) {
		//uiView.addAnnotations(checkpoints)
		
		//PER ABILITARE IL CORDINATOR
		//If you changing the Map Annotation then you have to remove old Annotations
		//uiView.removeAnnotations(uiView.annotations)
		//assigning delegate
		uiView.delegate = context.coordinator
		//passing model array here
		uiView.addAnnotations(checkpoints)
		
		//	uiView.addAnnotations(pizzaAnnotations.restaurants)
		
	}
	
	
	/* Senza passaggio di array, nel senso che richiedi tutte le posizioni tramite la funzione requestMockData()
	let landmarks = LandmarkAnnotation.requestMockData()
	
	func makeCoordinator() -> MapViewCoordinator {
	MapViewCoordinator(self)
	}
	
	/**
	- Description - Replace the body with a make UIView(context:) method that creates and return an empty MKMapView
	*/
	func makeUIView(context: Context) -> MKMapView{
	MKMapView(frame: .zero)
	}
	
	func updateUIView(_ view: MKMapView, context: Context){
	//If you changing the Map Annotation then you have to remove old Annotations
	//mapView.removeAnnotations(mapView.annotations)
	view.delegate = context.coordinator
	view.addAnnotations(landmarks)
	}
	
	*/
	
	
	
}

class MapViewCoordinator: NSObject, MKMapViewDelegate {
	
	var view: MapView
	@ObservedObject var viewRouter2: ViewRouter2
	
	init(_ control: MapView, viewRouter2: ViewRouter2) {
		self.view = control
		self.viewRouter2 = viewRouter2
		
	}
	
	private let reuseIdentifier = "MyIdentifier"
	
	
	
	
	func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
		
		if annotation is MKUserLocation { return nil }
		
		var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier) as? MKPinAnnotationView
		
		annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
		
		//è la mia posizione
		if annotationView == nil {
			
		} else {
			
			//immagine del pin
			//annotationView?.image = UIImage(named: "locationPin")
			//Il pop message in alto
			annotationView?.canShowCallout = true
			annotationView?.annotation = annotation
			//colore dei pin
			annotationView?.pinTintColor = .green
			
			
			
			
			
			
			let rightButton = UIButton(type: .detailDisclosure)
			//rightButton.setImage( UIImage(named: ""), for: .normal)
			rightButton.addTarget(self, action: #selector(MapViewCoordinator.buttonAction(_:)), for: .touchUpInside)

			
			
			
			annotationView?.rightCalloutAccessoryView = rightButton
			
			
		}
		return annotationView
	}
	
	func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
		
		let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier) as? MKPinAnnotationView
		
		
		
		
		//Solo i pin dei campi, (non la mia posizione)
		if annotationView == nil {
			print("cliccato")
		}
		
	}
	
	func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
		// Open view here
		
		// You can also get added annotation
		let annotationView = views.first
		print(annotationView?.annotation?.title)
		
		
		//self.viewRouter.currentPage = "page3"
	}
	
	
	@objc func buttonAction(_ sender:UIButton!){
		self.viewRouter2.currentPage = "page2"
		/*NavigationLink(destination:FieldDetail(field:fieldData[0],event:fieldData[0].events[0],viewRouter: viewRouter) ) {
			Image(systemName: "plus.circle.fill")
				.font(.largeTitle)
		}.foregroundColor(Color("DarkGreen"))
*/
		
		
		
		
		
		
		
		
	}
	
	
	
	
	
}



/*
enum FieldType {
case type1
case type2
case type3
}

class PizzaAnnotation:NSObject,MKAnnotation{
var coordinate: CLLocationCoordinate2D
var title: String?
var subtitle: String?
var type: FieldType
init(_ latitude:CLLocationDegrees,_ longitude:CLLocationDegrees,title:String,subtitle:String,type:FieldType){
self.coordinate = CLLocationCoordinate2DMake(latitude, longitude)
self.title = title
self.subtitle = subtitle
self.type = type
}

}


class PizzaAnnotations: NSObject {
var restaurants:[PizzaAnnotation]

override init(){
//build an array of pizza loactions literally
restaurants = [PizzaAnnotation(41.892472,-87.62687676, title: "Campo 1", subtitle:"First Deep Dish Pizza", type: .type1)]
restaurants += [PizzaAnnotation(41.8931164,-87.6267778, title: "Campo 2", subtitle:"Second Uno Location", type: .type1)]
restaurants += [PizzaAnnotation(41.8957338,-87.6229457, title: "Campo 3", subtitle:"Deep Dish if you can't cross Michigan Avenue", type: .type2)]
restaurants += [PizzaAnnotation(41.9206924,-87.6375364, title: "Campo 4", subtitle:"Pizza baked in a bowl", type: .type3)]
restaurants += [PizzaAnnotation(41.9217837,-87.6645778, title: "Campo 5", subtitle:"Carmelized Crust Deep Dish", type: .type2)]
restaurants += [PizzaAnnotation(42.0018732,-87.7258586 , title: "Campo 6", subtitle:"Ex Uno's Manager goes solo", type: .type2)]
restaurants += [PizzaAnnotation(41.8910953,-87.6597941 , title: "Campo 7", subtitle:"Coal fired thin pizza", type: .type1)]
restaurants += [PizzaAnnotation(41.9105463,-87.6760223, title: "Campo 8", subtitle:"White, thin square pizza", type: .type1)]
restaurants += [PizzaAnnotation(41.9633682,-87.6737948, title: "Campo 9", subtitle:"Authentic Neopolitan pizza", type: .type1)]
}
}
*/

class LandmarkAnnotation: NSObject, MKAnnotation {
	let title: String?
	let subtitle: String?
	let coordinate: CLLocationCoordinate2D
	
	init(title: String?,
		  subtitle: String?,
		  coordinate: CLLocationCoordinate2D) {
		self.title = title
		self.subtitle = subtitle
		self.coordinate = coordinate
	}
	
	/*
	static func requestMockData()-> [LandmarkAnnotation]{
	return [
	LandmarkAnnotation(title: "Bengalore",
	subtitle:"Bengaluru (also called Bangalore) is the capital of India's southern Karnataka state.",
	coordinate: .init(latitude: 12.9716, longitude: 77.5946)),
	LandmarkAnnotation(title: "Mumbai",
	subtitle:"Mumbai (formerly called Bombay) is a densely populated city on India’s west coast",
	coordinate: .init(latitude: 19.0760, longitude: 72.8777))
	]
	}
	*/
}



