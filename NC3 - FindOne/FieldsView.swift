//
//  FieldsView.swift
//  NC3 - FindOne
//
//  Created by Maurizio Minieri on 03/03/2020.
//  Copyright © 2020 Maurizio Minieri. All rights reserved.
//
import SwiftUI
import MapKit

struct FieldsView: View {
	
	@ObservedObject var viewRouter2: ViewRouter2
	@State private var searchText = ""
	@State private var showCancelButton: Bool = false
	
	//fields in cui già è stato creato un evento (visibili nel tasto fields)
	@State var checkpoints: [LandmarkAnnotation] = [
		LandmarkAnnotation(title: "Napoli2", subtitle: "Bello2", coordinate: .init(latitude: 40.8765005, longitude: 14.1303887)),
		LandmarkAnnotation(title: "Stadio", subtitle: "Che figooooo, che campo wauuu", coordinate: .init(latitude: 40.828845, longitude: 15.194173))
	]
	
	//fields in cui creare un evento, (visibili col tasto +)
	@State var checkpointsPlus: [LandmarkAnnotation] = [
		LandmarkAnnotation(title: "Napoli", subtitle: "Bello", coordinate: .init(latitude: 40.8702996, longitude: 15.1679429)),
		LandmarkAnnotation(title: "Mergellina", subtitle: "Bello wau che bello sto campo, waaa", coordinate: .init(latitude: 40.834506, longitude: 15.237417))
	]
	
	
	
	
	var body: some View {
		
		VStack{
			
			//apriremo la mapview con tutti i checkpoint dei campi con dei posti disonibili
			MapView(viewRouter2: viewRouter2, checkpoints: $checkpoints)
			
			
			//.edgesIgnoringSafeArea(.top)
			//.frame(height: 300)
		}.navigationBarTitle("Fields")
		
	}
}


struct FieldsView_Previews: PreviewProvider {
	static var previews: some View {
		ForEach(["iPhone SE", "iPhone Xs Max", "iPad Pro (11-inch)"], id: \.self) { deviceName in
			
			Group{
				FieldsView(viewRouter2: ViewRouter2())
					.previewDevice(PreviewDevice(rawValue: deviceName))
					.environment(\.colorScheme, .light)
					.previewDisplayName(deviceName)
				
				FieldsView(viewRouter2: ViewRouter2())
					.previewDevice(PreviewDevice(rawValue: deviceName))
					.environment(\.colorScheme, .dark)
					.previewDisplayName(deviceName)
			}
		}
	}
	
}

