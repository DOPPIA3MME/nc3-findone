//
//  Data.swift
//  NC3 - FindOne
//
//  Created by Maurizio Minieri on 02/03/2020.
//  Copyright © 2020 Maurizio Minieri. All rights reserved.
//

import UIKit
import SwiftUI
import CoreLocation

//properties
let fieldData: [Field] = load("fieldData.json")
let eventData: [Event] = load("eventData2.json")

func load<T: Decodable>(_ filename: String) -> T {
	let data: Data
	
	guard let file = Bundle.main.url(forResource: filename, withExtension: nil)
		else {
			fatalError("Couldn't find \(filename) in main bundle.")
	}
	
	do {
		data = try Data(contentsOf: file)
	} catch {
		fatalError("Couldn't load \(filename) from main bundle:\n\(error)")
	}
	
	do {
		let decoder = JSONDecoder()
		return try decoder.decode(T.self, from: data)
	} catch {
		fatalError("Couldn't parse \(filename) as \(T.self):\n\(error)")
	}
}

final class ImageStore {
	typealias _ImageDictionary = [String: CGImage]
	fileprivate var images: _ImageDictionary = [:]
	
	fileprivate static var scale = 2
	
	static var shared = ImageStore()
	
	func image(name: String) -> Image {
		let index = _guaranteeImage(name: name)
		
		return Image(images.values[index], scale: CGFloat(ImageStore.scale), label: Text(name))
	}
	
	static func loadImage(name: String) -> CGImage {
		guard
			let url = Bundle.main.url(forResource: name, withExtension: "jpg"),
			let imageSource = CGImageSourceCreateWithURL(url as NSURL, nil),
			let image = CGImageSourceCreateImageAtIndex(imageSource, 0, nil)
			else {
				fatalError("Couldn't load image \(name).jpg from main bundle.")
		}
		return image
	}
	
	fileprivate func _guaranteeImage(name: String) -> _ImageDictionary.Index {
		if let index = images.index(forKey: name) { return index }
		
		images[name] = ImageStore.loadImage(name: name)
		return images.index(forKey: name)!
	}
}





//MARK: - GET & POST

/*
protocol HomeModelProtocol: class {
func itemsDownloaded(items: NSArray)
}


class HomeModel: NSObject, URLSessionDataDelegate {

//properties

weak var delegate: HomeModelProtocol!

let urlPath = "https://findone.altervista.org/service.php" //this will be changed to the path where service.php lives

func getData() {

let url: URL = URL(string: urlPath)!
let defaultSession = Foundation.URLSession(configuration: URLSessionConfiguration.default)

let task = defaultSession.dataTask(with: url) { (data, response, error) in

if error != nil {
print("Failed to download data")
}else {
print("Data downloaded")
self.parseJSON(data!)
}

}

task.resume()
}

func parseJSON(_ data:Data) {

var jsonResult = NSArray()

do{
jsonResult = try JSONSerialization.jsonObject(with: data, options:JSONSerialization.ReadingOptions.allowFragments) as! NSArray

} catch let error as NSError {
print(error)

}

var jsonElement = NSDictionary()
let locations = NSMutableArray()

for i in 0 ..< jsonResult.count
{

jsonElement = jsonResult[i] as! NSDictionary

let location = LocationModel()

//the following insures none of the JsonElement values are nil through optional binding
if let id = jsonElement["id"] as? String
{
print(id)
}
if let name = jsonElement["name"] as? String
{
print(name)
location.name = name
}
if let surname = jsonElement["surname"] as? String
{
print(surname)
location.surname = surname
}
if let email = jsonElement["email"] as? String
{
print(email)
location.email = email
}
if let cellNumber = jsonElement["cellNumber"] as? String
{
print(cellNumber)
location.cellNumber = cellNumber
}



locations.add(location)

}


//postData()
}


//HTTP POST
func postData(){


// HTTP Request Parameters which will be sent in HTTP Request Body
var postString = ""
postString = postString + "name=Mara" // add items as name and value
postString = postString + "&surname=Maionchi"
postString = postString + "&email=figa@gmail.com"
postString = postString + "&cellNumber=3335368123"


let url = URL(string: "https://findone.altervista.org/insert-service.php?\(postString)")
guard let requestUrl = url else { fatalError() }
// Prepare URL Request Object
var request = URLRequest(url: requestUrl)
request.httpMethod = "POST"


// Set HTTP Request Body
//request.httpBody = postString.data(using: String.Encoding.utf8)  INUTILE
print("REQUEST:\(request)")
//print(postString.data(using: String.Encoding.utf8))
// Perform HTTP Request
let task = URLSession.shared.dataTask(with: request) { (data, response, error) in

// Check for Error
if let error = error {
print("Error took place \(error)")
return
}

// Convert HTTP Response Data to a String
if let data = data, let dataString = String(data: data, encoding: .utf8) {
print("Response data string:\n \(dataString)")
}
}
task.resume()

}


//Versione Json (potrebbe essere utile (?)
func postDataJson()
{

// prepare json data
let json: [String: Any] = ["name": "Bigb",
"surname": "Minieri",
"email": "m@g.it",
"cellNumber": "3"
]

let jsonData = try? JSONSerialization.data(withJSONObject: json)

// create post request
let url = URL(string: "https://findone.altervista.org/insert-service.php")!
var request = URLRequest(url: url)
request.httpMethod = "POST"

// insert json data to the request
request.httpBody = jsonData

let task = URLSession.shared.dataTask(with: request) { data, response, error in
guard let data = data, error == nil else {
print(error?.localizedDescription ?? "No data")
return
}
let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
if let responseJSON = responseJSON as? [String: Any] {
print(responseJSON)
}
}

task.resume()
}
}

*/
