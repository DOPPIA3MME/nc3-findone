//
//  LandmarkList.swift
//  NC3 - FindOne
//
//  Created by Maurizio Minieri on 02/03/2020.
//  Copyright © 2020 Maurizio Minieri. All rights reserved.
//

import SwiftUI

struct MyEventList: View {
	
	@ObservedObject var viewRouter: ViewRouter
	@ObservedObject var viewRouter2: ViewRouter2
	
	//fields in cui creare un evento, (visibili col tasto +)
	@State var checkpointsPlus: [LandmarkAnnotation] = [
		LandmarkAnnotation(title: "Napoli", subtitle: "Bello", coordinate: .init(latitude: 40.8702996, longitude: 14.1679429)),
		LandmarkAnnotation(title: "Mergellina", subtitle: "Bello wau che bello sto campo, waaa", coordinate: .init(latitude: 40.834506, longitude: 14.237417))
	]
	
	
	
	@State var showingProfile = false
	
	var profileButton: some View {
		Button(action: { self.showingProfile.toggle() }) {
			Image(systemName: "person.crop.circle")
				.imageScale(.large)
				.accessibility(label: Text("User Profile2"))
				.padding()
		}
	}
	
	var plusButton: some View {
		NavigationLink(destination: MapView(viewRouter2:viewRouter2, checkpoints: $checkpointsPlus)) {
			Image(systemName: "plus.circle.fill")
				.font(.largeTitle)
		}.foregroundColor(Color("DarkGreen"))//.buttonStyle(PlainButtonStyle())
	}
	
	var body: some View {
		NavigationView{
		List{
			ForEach(fieldData) { field in
				Text(field.name).font(.headline)
				ForEach(field.events){ event in
					NavigationLink(destination: EventDetail(field: field,event: event)) {
						EventRow(field: field, event: event)
					}
					.frame(minWidth: 0, maxWidth: .infinity)
					.padding()
					.foregroundColor(.white)
					.background(LinearGradient(gradient: Gradient(colors: [Color("DarkGreen"), Color("LightGreen")]), startPoint: .leading, endPoint: .trailing))
					.cornerRadius(40)
				}
			}
		}	.navigationBarTitle(Text("Events"))
			.navigationBarItems(trailing: plusButton)
			.sheet(isPresented: $showingProfile) {
				Text("User Profile")
			}
		}
		
	}
	
}

struct LandmarkList_Previews: PreviewProvider {
	static var previews: some View {
		ForEach(["iPhone SE", "iPhone Xs Max", "iPad Pro (11-inch)"], id: \.self) { deviceName in
			
			Group{
				MyEventList(viewRouter: ViewRouter(), viewRouter2: ViewRouter2())
					.previewDevice(PreviewDevice(rawValue: deviceName))
					.environment(\.colorScheme, .light)
					.previewDisplayName(deviceName)
				
				MyEventList(viewRouter: ViewRouter(), viewRouter2: ViewRouter2())
					.previewDevice(PreviewDevice(rawValue: deviceName))
					.environment(\.colorScheme, .dark)
					.previewDisplayName(deviceName)
			}
		}
	}
}



