//
//  UserData.swift
//  NC3 - FindOne
//
//  Created by Maurizio Minieri on 10/03/2020.
//  Copyright © 2020 Maurizio Minieri. All rights reserved.
//

import Combine
import SwiftUI

final class UserData: ObservableObject {
	@Published var showFavoritesOnly = false
	@Published var fields = fieldData
}
